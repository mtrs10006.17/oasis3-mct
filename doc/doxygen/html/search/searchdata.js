var indexSectionsWithContent =
{
  0: "acdefgijklmnoprstvw",
  1: "opt",
  2: "m",
  3: "mo",
  4: "acdfgimnops",
  5: "acdefgijklmnoprstvw",
  6: "n",
  7: "o"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "defines",
  7: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Macros",
  7: "Pages"
};

