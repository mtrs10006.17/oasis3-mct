#!/bin/ksh
#set -xv
######################################################################
#
host=`uname -n`
user=`whoami`
#
## - Define paths
srcdir=`pwd`
datadir=$srcdir/data_oasis3
casename=`basename $srcdir`
#
## - Name of the executables
    exe1=model1
    exe2=model2
#
############### User's section #######################################
#
#
## - Define architecture and coupler 
arch=training_computer   #training_computer/Linux pgi openmpi/Linux gfortran openmpi
                    #nemo_lenovo intel impi, kraken intel impi, beaufix intel impi, 
		    #ada, Cray, machine_test 
#
# - Define number of processes to run each executable
    nproc_exe1=1
    nproc_exe2=1
#
## - Define rundir
if [ ${arch} == training_computer ]; then
    MPIRUN=/usr/local/intel/impi/2018.1.163/bin64/mpirun
    rundir=${HOME}/OASIS3-MCT_4.0/oasis3-mct/examples/${casename}/work_${casename}_${exe1}_${nproc_exe1}_${exe2}_${nproc_exe2}
# Stiff pgi 18.7
elif [ ${arch} == linux_pgi_openmpi ] || [ ${arch} == linux_pgi_openmpi_openmp ]; then
    MPIRUN=/usr/local/pgi/linux86-64/18.7/mpi/openmpi-2.1.2/bin/mpirun
    rundir=$srcdir/work_${casename}_${exe1}_${nproc_exe1}_${exe2}_${nproc_exe2}
# Fundy gfortran
elif [ ${arch} == linux_gfortran_openmpi ] || [ ${arch} == linux_gfortran_openmpi_openmp ]; then
    MPIRUN=/usr/lib64/openmpi/bin/mpirun
    rundir=$srcdir/work_${casename}_${exe1}_${nproc_exe1}_${exe2}_${nproc_exe2}
# Nemo_lenovo intel impi
elif [ ${arch} == nemo_lenovo_intel_impi ] || [ ${arch} == nemo_lenovo_intel_impi_openmp ]; then
    rundir=$srcdir/work_${casename}_${exe1}_${nproc_exe1}_${exe2}_${nproc_exe2}
# Kraken intel impi
elif [ ${arch} == kraken_intel_impi ] || [ ${arch} == kraken_intel_impi_openmp ]; then
    rundir=$srcdir/work_${casename}_${exe1}_${nproc_exe1}_${exe2}_${nproc_exe2}
# Beaufix M-F intel impi
elif [ ${arch} == beaufix_intel_impi ] || [ ${arch} == beaufix_intel_impi_openmp ]; then
    rundir=$srcdir/work_${casename}_${exe1}_${nproc_exe1}_${exe2}_${nproc_exe2}
elif [ ${arch} == ada ]; then
  MPIRUN=/opt/intel/impi/4.1.0.024/intel64/bin/mpirun
  rundir=/workgpfs/rech/ces/rces980/modBRETAGNE/oasis3-mct/examples/tutorial/work_${casename}_${exe1}_${nproc_exe1}_${exe2}_${nproc_exe2}
elif [ ${arch} == ukmo_cray_xc40 ] || [ ${arch} == ukmo_cray_xc40_rose ]; then
    rundir=$PWD/runDir
# Machine_test (for developments, here tioman pgi 17.10)
elif [ ${arch} == machine_test ]; then
    MPIRUN=/usr/local/pgi/linux86-64/17.10/mpi/openmpi-2.1.2/bin/mpirun
    rundir=$srcdir/work_${casename}_${exe1}_${nproc_exe1}_${exe2}_${nproc_exe2}
fi
#
############### End of user's section ################################

echo ''
echo '*****************************************************************'
echo '*** '$casename' : '$run
echo ''
echo 'Rundir       :' $rundir
echo 'Architecture :' $arch
echo 'Host         : '$host
echo 'User         : '$user
echo ''
echo $exe1' runs on '$nproc_exe1 'processes'
echo $exe2' runs on '$nproc_exe2 'processes'
echo ''
echo ''
######################################################################
###
### 1. Copy source example directory containing everything needed
###    into rundir

\rm -fr $rundir
mkdir -p $rundir

cp -f $datadir/*nc  $rundir/.
cp -f $datadir/*.jnl $rundir/.

cp -f $srcdir/$exe1 $rundir/.
cp -f $srcdir/$exe2 $rundir/.

cp -f $datadir/namcouple $rundir/.
#
cd $rundir


######################################################################
###
### 3. Creation of configuration scripts
###
###---------------------------------------------------------------------
### NEMO_LENOVO_INTEL IMPI
###---------------------------------------------------------------------
if [ ${arch} == nemo_lenovo_intel_impi ] || [ ${arch} == nemo_lenovo_intel_impi_openmp ]; then

  cat <<EOF > $rundir/run_$casename.$arch
#!/bin/bash -l
#Partition
#SBATCH --partition prod
# Nom du job
#SBATCH --job-name $casename
# Temps limite du job
#SBATCH --time=01:00:00
#SBATCH --output=$rundir/$casename.o
#SBATCH --error=$rundir/$casename.e
# Nombre de noeuds et de processus MPI
#SBATCH --nodes=1 --ntasks-per-node=24
#SBATCH --distribution cyclic

cd $rundir

ulimit -s unlimited
module purge
module -s load compiler/intel/2015.2.164 mkl/2015.2.164 mpi/intelmpi/5.0.3.048
#
time mpirun -np $nproc_exe1 ./$exe1 : -np $nproc_exe2 ./$exe2
#
EOF

###---------------------------------------------------------------------
### KRAKEN_INTEL IMPI
###---------------------------------------------------------------------
elif [ ${arch} == kraken_intel_impi ] || [ ${arch} == kraken_intel_impi_openmp ]; then

  (( nproc = $nproc_exe1 + $nproc_exe2 ))

  cat <<EOF > $rundir/run_$casename.$arch
#!/bin/bash -l
#SBATCH --partition prod
# Nom du job
#SBATCH --job-name $casename
# Temps limite du job
#SBATCH --time=01:00:00
#SBATCH --output=$rundir/$casename.o
#SBATCH --error=$rundir/$casename.e
# Nombre de noeuds et de processus
#SBATCH --nodes=$nnode --ntasks-per-node=36
#SBATCH --distribution cyclic

cd $rundir

ulimit -s unlimited
module purge
module load compiler/intel/18.0.1.163
module load mpi/intelmpi/2018.1.163
module load lib/netcdf-fortran/4.4.4_impi
module load lib/netcdf-c/4.6.1_impi
#
#
time mpirun -np $nproc_exe1 ./$exe1 : -np $nproc_exe2 ./$exe2
#
EOF
  
###---------------------------------------------------------------------
### BEAUFIX INTEL IMPI
###---------------------------------------------------------------------
elif [ $arch == beaufix_intel_impi ] || [ $arch == beaufix_intel_impi_openmp ]; then

   (( nproc = $nproc_exe1 + $nproc_exe2 ))

  cat <<EOF > $rundir/run_$casename.$arch
#!/bin/bash
#SBATCH --time=01:00:00
#SBATCH --partition=normal64 # queue
#SBATCH --job-name=toys     # job name
#SBATCH -N 1                # number of nodes
#SBATCH -n $nproc                # number of procs
#SBATCH -o job.out%j
#SBATCH -o job.err%j
#SBATCH --exclusive

ulimit -s unlimited
cd $rundir
module load intel/16.1.150
module load intelmpi/5.1.2.150
module load netcdf/4.4.0 
#
# Activate next line to run in coupled mode
time $MPIRUN -np $nproc_exe1 ./$exe1 : -np $nproc_exe2 ./$exe2
#
EOF

###---------------------------------------------------------------------
### ada
###---------------------------------------------------------------------
elif [ $arch == ada ] ; then

 (( nproc = $nproc_exe1 + $nproc_exe2 ))

  cat <<EOF > $rundir/run_$casename.$arch
#!/bin/ksh
# ######################
# ##   ADA IDRIS   ##
# ######################
# Nom de la requete
# @ job_name = ${casename}
# Type de travail
# @ job_type = parallel
# Fichier de sortie standard
# @ output = Script_Output_${casename}.000001
# Fichier de sortie erreur (le meme)
# @ error = Script_Output_${casename}.000001
# Nombre de processus demandes
# @ total_tasks = ${nproc}
# @ environment = "BATCH_NUM_PROC_TOT=32"
# Temps CPU max. par processus MPI hh:mm:ss
# @ wall_clock_limit = 0:30:00
# Fin de l entete
# @ queue
#
# pour avoir l'echo des commandes
set -x

# on se place dans le repertoire rundir
cd ${rundir}

module load netcdf
module load hdf5

#
export KMP_STACKSIZE=64m
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/smplocal/pub/NetCDF/4.1.3/lib

poe -pgmmodel MPMD -cmdfile run_file
#
EOF

###---------------------------------------------------------------------
### UKMO_CRAY_XC40 OR UKMO_CRAY_XC40_ROSE
###---------------------------------------------------------------------
elif [ ${arch} == ukmo_cray_xc40 ]; then

     (( nproc = $nproc_exe1 + $nproc_exe2 ))
echo writing arch file to $rundir/run_$casename.$arch

 cat <<EOF > $rundir/run_$casename.$arch
#!/bin/bash --login
#PBS -N ${casename}
#PBS -l walltime=00:10:00
#PBS -l select=2
#PBS -q normal

cd $rundir

module load cray-netcdf-hdf5parallel/4.3.2

aprun -n $nproc_exe1 ./$exe1 : -n $nproc_exe2 ./$exe2

EOF

elif [ ${arch} == ukmo_cray_xc40_rose ]; then

    (( nproc = $nproc_exe1 + $nproc_exe2 ))
echo writing arch file to $rundir/run_$casename.$arch

 cat <<EOF > $rundir/run_$casename.$arch
#!/bin/bash 

cd $rundir

module load cray-netcdf-hdf5parallel/4.3.2

aprun -n $nproc_exe1 ./$exe1 : -n $nproc_exe2 ./$exe2

EOF
chmod u+x $rundir/run_$casename.$arch
fi 

######################################################################
###
### 4. Execute the model

if [ $arch == training_computer ]; then
    export OMP_NUM_THREADS=1
    echo 'Executing the model using '$MPIRUN 
    $MPIRUN -np $nproc_exe1 ./$exe1 : -np $nproc_exe2 ./$exe2 > runjob.err
elif [ $arch == linux_pgi_openmpi ] || [ $arch == linux_pgi_openmpi_openmp ] || [ $arch == linux_gfortran_openmpi ] || [ ${arch} == linux_gfortran_openmpi_openmp ] || [ $arch == machine_test ]; then
    export OMP_NUM_THREADS=1
    $MPIRUN -np $nproc_exe1 ./$exe1 : -np $nproc_exe2 ./$exe2 > runjob.err   
elif [ ${arch} == nemo_lenovo_intel_impi ]  || [ ${arch} == nemo_lenovo_intel_impi_openmp ] || [ ${arch} == kraken_intel_impi ]  || [ ${arch} == kraken_intel_impi_openmp ] || [ $arch == beaufix_intel_impi ] || [ ${platform} == beaufix_intel_impi_openmp ]; then
    echo 'Submitting the job to queue using sbatch'
    sbatch $rundir/run_$casename.$arch
    squeue -u $USER
elif [ $arch == ada ]; then
    echo 'Submitting the job to queue using llsubmit'
    llsubmit $rundir/run_$casename.$arch
elif [ ${arch} == ukmo_cray_xc40 ]; then
    qsub $rundir/run_$casename.$arch
elif [ ${arch} == ukmo_cray_xc40_rose ]; then
    $rundir/run_$casename.$arch
fi


echo $casename 'is executed or submitted to queue.'
echo 'Results are found in rundir : '$rundir 

######################################################################
